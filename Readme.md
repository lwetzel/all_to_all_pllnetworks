This code has been tested and run with: Python 3.6.4 :: Anaconda, Inc.

### How to run a simulation

The simulations for networks of all-to-all mutually delay-coupled phase-locked loops can be run using the start.py script.
```ruby
cd <foldername>
python start.py
```
The file **start.py** also contains all the parameters that define the network of phase-locked loops.<br>
The parameters for the simulation are:

```
init_phases  = 'iid'     # specifies the distribution of initial phases of the PLLs [zero, gaussian, iid]
init_hist    = 'free'    # specifies initial history for the simulation             [free, synced]
int_frequen  = 'lorentz' # specifies distribution of intrinsic frequencies          [const, gaussian, lorentz]
N            = 10000;    # number of PLLs in the system, coupling is all-to-all
Tsim         = 1000;     # simulation time in [seconds]
Kstart       = 0.019;    # starting coupling strength in [Hz], necessary if adiabatic != 2
Kend         = 0.035;    # maximum coupling strength in [Hz],  necessary if adiabatic != 2
fc           = 1.5915;   # cut-off frequency in [Hz]                                [fc=1/(2*pi*m)]
tau          = 2.0;      # transmission delay in [seconds]
m_init_phase = 3.14;     # mean initial phases of PLLs [rad]
var_init_phi = 6.28;     # variance of gaussian or iid distribution of initial phases
m_int_freqs  = 0.4779;   # mean intrinsic frequency of the PLLs in [Hz]
v_int_freqs  = 0.0159;   # variance of gaussian or scale of lorentz distributed int. frequencies
sync_freq    = 0.4779;   # initial frequency of the system in case of coupled initial state [Hz]
SamplesPerT  = 125;      # samples per period, determines time-step for iteration of Euler-sheme
adiabatic    = 2;        # whether (1 & 2) or not (0) adiabatic change of variable, 1: real adiabatic mode, 2: K(n+1)=K(n)+dK
ad_rate      = 0.0005;   # adiabatic rate of change for the coupling strength, yields: Krate = ad_rate / Tsim
downscale    = 10;       # sample rate when plotting and saving data, i.e., downscale 10 saves and plots only every 10th value
plot_phi_freq= 0;        # whether to plot phases and frequencies
plot_out     = 1;        # whether to open plots in matplotlib windows
wrap_hist_tau= 1;        # whether time-series wrapped into vector of length tau --> evolveOrderReduced vs evolveOrder
```

The 'Kvalues_vector' contains the values of K that are simulated subsequently in 'adiabatic' mode 2, in any other mode,<br>
the K-values are set by 'Kstart' and 'Kend'

```ruby
Kvalues_vector = np.array([0.01909859, 0.01911454, 0.01913049, 0.01914644,
                           0.01917833, 0.01919428, 0.01921022, 0.01922617,
                           0.01925807, 0.01927401, 0.01928996, 0.01930591,
                           ....
                           0.03496625, 0.03498219, 0.03499814, 0.03501409]);
```

The results in the publication that links to this repository can be recovered by using this setup. The parameters
_init\_phases, init\_hist, int\_frequen, N, Tsim, fc, tau, m\_init\_phase, var\_init\_phi, m\_int\_freqs, v\int\_freqs, SmaplePerT_ can be changed to check different cases with the method that discretizes the change in coupling-strength _K_ and simulates for each of these values for a time _Tsim_. For other modes of simulation contact the maintainer please.

### How to plot the results in 'results.txt'

Use the load_and_plot.py script to plot the data.

### How to compile the python code


The python code can be compiled within the cythonize environment using the command:
```ruby
python setup.py build_ext --inplace
```
